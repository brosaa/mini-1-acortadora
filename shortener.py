import webapp
from urllib.parse import unquote


class Shortener (webapp.webApp):
    urls = {} #Diccionario vacio
    form = """
                <form action="/" method="POST">
                    <input type="text" name="url" placeholder="Complete url">
                    <input type="text" name="short" placeholder="Short url name"><br>
        	        <br>
    	        	<input type="submit" value="Enviar">
                </form>
    """

    @staticmethod
    def parse(request):
        try:
            method = request.split(' ', 2)[0]
            resource = request.split(' ', 2)[1]
            body = unquote(request.split('\r\n\r\n', 1)[1])
            return method, resource, body
        except ValueError:
            return "", "", ""

    def process(self, parsedinfo):
        method, resource, body = parsedinfo
        print(parsedinfo)
        if method == "GET":
            if resource == "/": #Enviar formulario
                httpCode = "200 OK"
                htmlBody = "<html><body>" + Shortener.form + "<br><br>" + "git p<p>Current dictionary:</p><br>" \
                           + str(self.urls) + "</body></html>"
            elif resource in self.urls: #Redireccionamiento
                httpCode = "308 Permanent Redirect"
                htmlBody = '<meta http-equiv="refresh" content="1;URL=' + self.urls[resource] + '">'

            else: #No existe la redirección en el diccionario
                httpCode = "404 Not Found"
                htmlBody = resource + " is not available"
            return httpCode, htmlBody

        if method == "POST":
            info = body.split('&')
            url = unquote(info[0].split('=')[1])
            if not url.startswith('http://') and not url.startswith('https://'): #Añadir https:// si no estaba
                url = "https://" + url
            short = '/' + info[1].split('=')[1]

            if url != "" and short != "/":
                self.urls[short] = url #Guardar url y short en el diccionario
                httpCode = "200 OK"
                htmlBody = "<html><body>" + Shortener.form + "<br><br>" + "<p>Current dictionary:</p><br>" \
                           + str(self.urls) + "</body></html>"
            else: #Algún cuadro del formulario estaba vacio
                httpCode = "404 Not Found"
                htmlBody = "<html><body>Fields are empty</body></html>"

            return httpCode, htmlBody

if __name__ == "__main__":
    testWebApp = Shortener("localhost", 1234)
